import Koa from 'koa'
import middleware from '~/server/middleware'
import mongoose from 'mongoose'
import { StudentModel } from '~/server/models/student'
import { TaskModel } from '~/server/models/task'
import request from 'supertest'

const app = new Koa()
const server = app.listen(3000, '127.0.0.1')

describe('Comment Controller', () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true
    })
    middleware(app)
  })

  afterEach(() => {
    server.close()
  })

  afterAll(async () => {
    await mongoose.connection.close()
  })

  it('Should return all tasks', async () => {
    await StudentModel.deleteMany({}).exec()
    await TaskModel.deleteMany({}).exec()

    const task = new TaskModel({
      startDate: new Date(),
      endDate: new Date().setHours(new Date().getHours() + 1),
      title: 'Test'
    })

    const anotherTask = new TaskModel({
      startDate: new Date(),
      endDate: new Date().setHours(new Date().getHours() + 1),
      title: 'Test2'
    })

    const student = new StudentModel({
      name: 'Test',
      surname: 'TestSurname',
      age: 10,
      tasks: [task],
      created_at: new Date(),
      updated_at: new Date()
    })

    const anotherStudent = new StudentModel({
      name: 'Test2',
      surname: 'TestSurname2',
      age: 11,
      tasks: [anotherTask],
      created_at: new Date(),
      updated_at: new Date()
    })

    await student.save().then(stud => {
      expect(stud).not.toBeNull()
      if (stud.tasks) {
        expect(stud.tasks.length).toEqual(1)
      } else {
        // should fail always
        expect(true).toEqual(false)
      }
    })
    await anotherStudent.save().then(stud => {
      expect(stud).not.toBeNull()
      if (stud.tasks) {
        expect(stud.tasks.length).toEqual(1)
      } else {
        // should fail always
        expect(true).toEqual(false)
      }
    })

    const response = await request(server).get('/tasks')

    expect(response.status).toEqual(200)
    expect(response.type).toEqual('application/json')
    expect(Array.isArray(response.body)).toEqual(true)
    expect(response.body.length).toEqual(2)
  })

  it("Should return only specified student's tasks", async () => {
    await StudentModel.deleteMany({}).exec()
    await TaskModel.deleteMany({}).exec()

    const task = new TaskModel({
      startDate: new Date(),
      endDate: new Date().setHours(new Date().getHours() + 1),
      title: 'Test'
    })

    const anotherTask = new TaskModel({
      startDate: new Date(),
      endDate: new Date().setHours(new Date().getHours() + 1),
      title: 'Test2'
    })

    const student = new StudentModel({
      name: 'Test',
      surname: 'TestSurname',
      age: 10,
      tasks: [task],
      created_at: new Date(),
      updated_at: new Date()
    })

    const anotherStudent = new StudentModel({
      name: 'Test2',
      surname: 'TestSurname2',
      age: 11,
      tasks: [anotherTask],
      created_at: new Date(),
      updated_at: new Date()
    })

    let idToGet

    await student.save().then(stud => {
      expect(stud).not.toBeNull()
      if (stud.tasks) {
        expect(stud.tasks.length).toEqual(1)
      } else {
        // should fail always
        expect(true).toEqual(false)
      }
      idToGet = stud.id
    })
    await anotherStudent.save().then(stud => {
      expect(stud).not.toBeNull()
      if (stud.tasks) {
        expect(stud.tasks.length).toEqual(1)
      } else {
        // should fail always
        expect(true).toEqual(false)
      }
    })

    const response = await request(server).get(`/tasks?id=${idToGet}`)

    expect(response.status).toEqual(200)
    expect(response.type).toEqual('application/json')
    expect(Array.isArray(response.body)).toEqual(true)
    expect(response.body.length).toEqual(1)
  })

  it('Should add new task', async () => {
    await StudentModel.deleteMany({}).exec()
    await TaskModel.deleteMany({}).exec()

    const student = new StudentModel({
      name: 'Test',
      surname: 'TestSurname',
      age: 10,
      created_at: new Date(),
      updated_at: new Date()
    })

    let idToGet

    await student.save().then(stud => {
      expect(stud).not.toBeNull()
      expect(stud.tasks && stud.tasks.length).toEqual(0)
      idToGet = stud.id
    })

    const response = await request(server)
      .post('/tasks')
      .send({
        studentId: idToGet,
        start: new Date(),
        end: new Date(),
        notes: 'Notes',
        title: 'Test'
      })

    expect(response.status).toEqual(200)
    expect(response.type).toEqual('application/json')
    expect(response.body.title).toEqual('Test')

    const anotherResponse = await request(server).get('/tasks')

    expect(anotherResponse.status).toEqual(200)
    expect(Array.isArray(anotherResponse.body)).toEqual(true)
    expect(anotherResponse.body[0].title).toEqual('Test')

    const studentAfter = await StudentModel.findById(idToGet).exec()
    expect(studentAfter).toHaveProperty('tasks')
    expect(
      studentAfter && studentAfter.tasks && studentAfter.tasks.length
    ).toEqual(1)
  })

  it('Should update task', async () => {
    await StudentModel.deleteMany({}).exec()
    await TaskModel.deleteMany({}).exec()

    const task = new TaskModel({
      startDate: new Date(),
      endDate: new Date().setHours(new Date().getHours() + 1),
      title: 'Test',
      notes: 'Initial notes'
    })

    const student = new StudentModel({
      name: 'Test',
      surname: 'TestSurname',
      age: 10,
      tasks: [task],
      created_at: new Date(),
      updated_at: new Date()
    })

    let idToGet
    let id

    await student.save().then(stud => {
      expect(stud).not.toBeNull()
      if (stud.tasks) {
        expect(stud.tasks.length).toEqual(1)
        id = stud.tasks ? (stud.tasks[0] as any)._id : null
      } else {
        // should fail always
        expect(true).toEqual(false)
      }
      idToGet = stud.id
    })

    const response = await request(server)
      .patch(`/tasks/${id}`)
      .send({
        studentId: idToGet,
        notes: 'Changed notes'
      })

    expect(response.status).toEqual(200)
    expect(response.type).toEqual('application/json')
    expect(response.body.tasks[0].notes).toEqual('Changed notes')

    const verifyStudent = await StudentModel.find({}).exec()
    const verifyTasks = verifyStudent[0].tasks || []
    const verifiedTask = verifyTasks[0]

    expect(verifiedTask.notes).toEqual('Changed notes')
    expect(verifiedTask.title).toEqual('Test')
  })

  it('Should delete task', async () => {
    await StudentModel.deleteMany({}).exec()
    await TaskModel.deleteMany({}).exec()

    const task = new TaskModel({
      startDate: new Date(),
      endDate: new Date().setHours(new Date().getHours() + 1),
      title: 'Test',
      notes: 'Initial notes'
    })

    const student = new StudentModel({
      name: 'Test',
      surname: 'TestSurname',
      age: 10,
      tasks: [task],
      created_at: new Date(),
      updated_at: new Date()
    })

    let studId
    let taskId

    await student.save().then(stud => {
      expect(stud).not.toBeNull()
      if (stud.tasks) {
        expect(stud.tasks.length).toEqual(1)
        taskId = stud.tasks ? (stud.tasks[0] as any)._id : null
      } else {
        // should fail always
        expect(true).toEqual(false)
      }
      studId = stud.id
    })

    const response = await request(server)
      .del(`/tasks/${taskId}`)
      .send({ studentId: studId })

    expect(response.status).toEqual(200)

    const verifyStudent = await StudentModel.findById(studId).exec()

    expect(verifyStudent).not.toBeNull()
    expect(
      verifyStudent && verifyStudent.tasks ? verifyStudent.tasks.length : -1
    ).toEqual(0)
  })
})
