import Koa from 'koa'
import middleware from '~/server/middleware'
import mongoose from 'mongoose'
import { StudentModel } from '~/server/models/student'
import request from 'supertest'

const app = new Koa()
const server = app.listen(3000, '127.0.0.1')

describe('Student Controller', () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true
    })
    middleware(app)
  })

  afterEach(() => {
    server.close()
  })

  afterAll(async () => {
    await mongoose.connection.close()
  })

  it('Should return all students', async () => {
    await StudentModel.deleteMany({}).exec()

    const student = new StudentModel({
      name: 'Test',
      surname: 'TestSurname',
      age: 10,
      created_at: new Date(),
      updated_at: new Date()
    })

    const anotherStudent = new StudentModel({
      name: 'Test2',
      surname: 'TestSurname2',
      age: 11,
      created_at: new Date(),
      updated_at: new Date()
    })

    await student.save().then(stud => expect(stud).not.toBeNull)
    await anotherStudent.save().then(stud => expect(stud).not.toBeNull)

    const response = await request(server).get('/students')

    expect(response.status).toEqual(200)
    expect(response.type).toEqual('application/json')
    expect(Array.isArray(response.body)).toBeTruthy()
    expect(response.body.length).toEqual(2)
  })

  it('Should add new student', async () => {
    await StudentModel.deleteMany({}).exec()

    const student = {
      nameInput: 'Test',
      surnameInput: 'Test Surname',
      ageInput: 10,
      cityInput: 'City',
      streetInput: 'Street',
      homeInput: 5,
      contactInput: 'Contact'
    }

    const response = await request(server)
      .post('/students')
      .send(student)

    expect(response.status).toEqual(200)
    expect(response.type).toEqual('application/json')
    expect(response.body).toHaveProperty('name')
    expect(response.body).toHaveProperty('surname')
    expect(response.body).toHaveProperty('age')
    expect(response.body).toHaveProperty('created_at')
    expect(response.body).toHaveProperty('updated_at')
  })

  it('Should update the student', async () => {
    await StudentModel.deleteMany({}).exec()

    const student = new StudentModel({
      name: 'Test',
      surname: 'TestSurname',
      age: 10,
      created_at: new Date(),
      updated_at: new Date()
    })

    await student.save().then(stud => expect(stud).not.toBeNull)

    const savedStudents = await StudentModel.find({}).exec()
    const savedStudent = savedStudents[0]

    const id = savedStudent ? savedStudent.id : expect(false).toBe(true)

    const studentUpdate = {
      nameInput: 'Test2',
      surnameInput: 'Test Surname',
      ageInput: 11,
      cityInput: 'City',
      streetInput: 'Street',
      homeInput: 5,
      contactInput: 'Contact'
    }

    const response = await request(server)
      .patch(`/students/${id}`)
      .send(studentUpdate)

    expect(response.status).toEqual(200)
    expect(response.type).toEqual('application/json')
    expect(response.body).toHaveProperty('age') // picked one of them randomly
    expect(response.body.age).toEqual(11) // the updated value
    expect(response.body.name).toEqual('Test2') // another updated value
    expect(response.body.home_number).toEqual(5) // and another one
    expect(response.body.surname).toEqual('Test Surname') // one of the old values
  })

  it('Should delete the student', async () => {
    await StudentModel.deleteMany({}).exec()

    const student = new StudentModel({
      name: 'Test',
      surname: 'TestSurname',
      age: 10,
      created_at: new Date(),
      updated_at: new Date()
    })

    await student.save().then(stud => expect(stud).not.toBeNull)

    const savedStudents = await StudentModel.find({}).exec()
    const savedStudent = savedStudents[0]

    const id = savedStudent ? savedStudent.id : expect(false).toBe(true)

    const response = await request(server).delete(`/students/${id}`)

    expect(response.status).toEqual(200)

    const leftOverStudents = await StudentModel.find({}).exec()

    expect(leftOverStudents.length).toEqual(0)
  })
})
