import mongoose from 'mongoose'
import request from 'supertest'
import Koa from 'koa'
import { CommentModel } from '../../server/models/comment'
import middleware from '~/server/middleware'

const app = new Koa()
const server = app.listen(3000, '127.0.0.1')

describe('Comment Controller', () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true
    })
    middleware(app)
  })

  afterEach(() => {
    server.close()
  })

  afterAll(async () => {
    await mongoose.connection.close()
  })

  it('Should return all sorted comments', async () => {
    await CommentModel.deleteMany({}).exec()

    const comment = new CommentModel({
      body: 'body',
      author: 'author',
      rating: 5
    })

    const anotherComment = new CommentModel({
      body: 'anotherBody',
      author: 'anotherAuthor',
      rating: 4
    })

    await comment.save().then(com => expect(com).not.toBeNull)
    await anotherComment.save(com => expect(com).not.toBeNull)

    const response = await request(server).get('/comments')
    expect(response.status).toEqual(200)
    expect(response.type).toEqual('application/json')
    expect(Array.isArray(response.body)).toEqual(true)
    expect(response.body.length).toEqual(2)
  })

  // not testing store since it requries captcha authentication

  it('Should delete a comment', async () => {
    await CommentModel.deleteMany({}).exec()

    const comment = new CommentModel({
      body: 'body',
      author: 'author',
      rating: 5
    })

    await comment.save()

    const initialComments = await CommentModel.find({}).exec()

    expect(initialComments.length).toEqual(1)

    const id = initialComments[0].id

    const response = await request(server).delete(`/comments/${id}`)

    expect(response.status).toEqual(200)

    const comments = await CommentModel.find({}).exec()

    expect(comments.length).toEqual(0)
  })
})
