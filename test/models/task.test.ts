import mongoose from 'mongoose'
import { TaskModel } from '../../server/models/task'
import { StudentModel } from '../../server/models/student'

describe('Task model', () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true
    })
  })

  afterAll(async () => {
    await mongoose.connection.close()
  })

  it('Should throw validation errors', () => {
    const task = new TaskModel()

    expect(task.validate).toThrow()
  })

  it('Should save a student', async () => {
    expect.assertions(3)

    const task = new TaskModel({
      startDate: Date(),
      endDate: Date(),
      notes: 'notes'
    })

    const student = new StudentModel({
      name: 'Test',
      surname: 'TestSurname',
      age: 10,
      created_at: new Date(),
      updated_at: new Date(),
      tasks: [task]
    })

    const spy = jest.spyOn(student, 'save')
    student.save()

    expect(spy).toHaveBeenCalled()

    await expect(student.tasks).not.toBeNull()
    await expect(student.tasks ? student.tasks.length : 0).toBe(1)
  })
})
