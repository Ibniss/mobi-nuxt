import mongoose from 'mongoose'
import { CommentModel } from '../../server/models/comment'

describe('Comment model', () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true
    })
  })

  afterAll(async () => {
    await mongoose.connection.close()
  })

  it('Should throw validation errors', () => {
    const comment = new CommentModel({
      body: 'some body',
      author: 'some author',
      rating: 6 // should be 0-5
    })

    expect(comment.validate).toThrow()
  })

  it('Should save a student', async () => {
    expect.assertions(3)

    const comment = new CommentModel({
      body: 'body',
      author: 'author',
      rating: 5
    })

    const spy = jest.spyOn(comment, 'save')
    comment.save()

    expect(spy).toHaveBeenCalled()

    expect(comment).toMatchObject({
      body: expect.any(String),
      author: expect.any(String),
      rating: expect.any(Number)
    })

    await expect(comment.body).toBe('body')
  })
})
