import mongoose from 'mongoose'
import { StudentModel } from '../../server/models/student'

describe('Student model', () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true
    })
  })

  afterAll(async () => {
    await mongoose.connection.close()
  })

  it('Should throw validation errors', () => {
    const student = new StudentModel()

    expect(student.validate).toThrow()
  })

  it('Should save a student', async () => {
    expect.assertions(3)

    const student = new StudentModel({
      name: 'Test',
      surname: 'TestSurname',
      age: 10,
      created_at: new Date(),
      updated_at: new Date()
    })

    const spy = jest.spyOn(student, 'save')
    student.save()

    expect(spy).toHaveBeenCalled()

    expect(student).toMatchObject({
      name: expect.any(String),
      surname: expect.any(String),
      age: expect.any(Number),
      created_at: expect.any(Date),
      updated_at: expect.any(Date)
    })

    await expect(student.surname).toBe('TestSurname')
  })
})
