import mongoose from 'mongoose'
import { UserModel } from '../../server/models/user'

describe('User model', () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true
    })
  })

  afterAll(async () => {
    await mongoose.connection.close()
  })

  it('Should throw validation errors', () => {
    const user = new UserModel()

    expect(user.validate).toThrow()
  })

  it('Should save a student', async () => {
    expect.assertions(3)

    const user = new UserModel({
      email: 'test@test.com',
      password: 'testpassword'
    })

    const spy = jest.spyOn(user, 'save')
    user.save()

    expect(spy).toHaveBeenCalled()

    expect(user).toMatchObject({
      email: expect.any(String),
      password: expect.any(String)
    })

    await expect(user.email).toBe('test@test.com')
  })
})
