const path = require('path')
const fs = require('fs')
const { MongoMemoryServer } = require('mongodb-memory-server')

const globalConfigPath = path.join(__dirname, 'globalConfig.json')

const mongod = new MongoMemoryServer({
  autoStart: false
})

module.exports = async () => {
  if (!mongod.isRunning) {
    await mongod.start()
  }

  const mongoConfig = {
    mongoDbName: 'jest',
    mongoUri: await mongod.getConnectionString()
  }

  // write global config to disk because all tests run in different contexts
  fs.writeFileSync(globalConfigPath, JSON.stringify(mongoConfig))

  // set global reference to mongod in order to close server during teardown
  global.__MONGOD__ = mongod
}
