import { prop, Typegoose } from 'typegoose'

class Comment extends Typegoose {
  @prop({ required: true })
  body: string
  @prop({ required: true })
  author: string
  @prop({ required: true, min: 0, max: 5 })
  rating: Number
}

export const CommentModel = new Comment().getModelForClass(Comment, {
  schemaOptions: {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  }
})
