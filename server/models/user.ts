import { prop, Typegoose } from 'typegoose'

const isEmail = value => {
  // eslint-disable-next-line no-useless-escape
  return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)
}
class User extends Typegoose {
  @prop({ required: true, unique: true, validate: value => isEmail(value) })
  email: string
  @prop({ required: true })
  password: string
}

export const UserModel = new User().getModelForClass(User)
