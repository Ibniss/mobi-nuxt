/* eslint-disable camelcase */
import { prop, Typegoose, arrayProp } from 'typegoose'
import { Task } from './task'

class Student extends Typegoose {
  @prop({ required: true })
  name: string
  @prop({ required: true })
  surname: string
  @prop({ required: true })
  age: Number
  @prop()
  city?: string
  @prop()
  street?: string
  @prop()
  home_number?: Number
  @prop()
  contact_details?: string
  @prop()
  created_at?: Date
  @prop()
  updated_at?: Date
  @arrayProp({ items: Task })
  tasks?: Task[]
}

export const StudentModel = new Student().getModelForClass(Student, {
  schemaOptions: {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  }
})
