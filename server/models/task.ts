import { prop, Typegoose } from 'typegoose'

export class Task extends Typegoose {
  @prop({ required: true })
  startDate: Date
  @prop({ required: true })
  endDate: Date
  @prop()
  notes: string
  @prop()
  title: string
}

export const TaskModel = new Task().getModelForClass(Task, {
  schemaOptions: {
    timestamps: true
  }
})
