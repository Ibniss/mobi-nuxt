/* eslint-disable no-console */
import mongoose from 'mongoose'

export default function setupDb() {
  mongoose.connect('mongodb://localhost:27017/dev', {
    useNewUrlParser: true
  })

  const db = mongoose.connection

  db.on('error', console.error.bind(console, 'connection error:'))
  db.once('open', () => {
    console.log('Connected')
  })
}
