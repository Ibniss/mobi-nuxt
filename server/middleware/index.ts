/* eslint-disable no-unused-vars */
import { IncomingMessage } from 'http'
import bodyParser from 'koa-body'
import cors from '@koa/cors'
import session from 'koa-session'
import Koa from 'koa'
import router from '../routes'

// class NuxtMessage extends IncomingMessage {
//   serverData: object

//   constructor(req: IncomingMessage, data: object) {
//     super(req.socket)
//     this.serverData = data
//   }
// }

export default function middlewares(app: Koa): void {
  // add error handling
  app.use(async (ctx, next) => {
    try {
      await next()

      if (ctx.status === 404 && ctx.res.headersSent === false) {
        ctx.throw(404)
      }
    } catch (err) {
      ctx.status = err.status || 500

      try {
        // write nuxt middleware to handle errors?
        // await nuxtMiddleware(ctx)
      } catch (nuxtError) {
        ctx.type = 'json'
        ctx.body = {
          status: ctx.status,
          message: err.message
        }

        // ctx.app.emit('error', err, ctx)
      }
    }
  })

  // add sessions support
  app.use(
    session(
      {
        // @ts-ignore
        autoCommit: false
      },
      app
    )
  )

  // serve static files
  // app.use(serve(config.static_dir.root))

  // parse json request bodies
  app.use(
    bodyParser({ parsedMethods: ['GET', 'POST', 'PATCH', 'PUT', 'DELETE'] })
  )

  // add CORS
  app.use(cors())

  // add api routes
  app.use(router.routes())
  app.use(router.allowedMethods())

  // add messages from the session to the request to pass to Nuxt
  app.use(async (ctx, next) => {
    // ctx.req = new NuxtMessage(ctx.req, {
    //   serverData: ctx.session.serverData || {}
    // })

    // @ts-ignore
    ctx.req.serverData = ctx.session.serverData || {}
    delete ctx.session.serverData
    await ctx.session.manuallyCommit()
    await next()
  })
}
