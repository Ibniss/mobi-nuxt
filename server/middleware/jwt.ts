import { config } from 'dotenv'
import koaJwt from 'koa-jwt'

config()
const jwt = koaJwt({
  secret: process.env.JWT_TOKEN || ''
})
export default jwt
