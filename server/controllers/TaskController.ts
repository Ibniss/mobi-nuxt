/* eslint-disable no-unused-vars */
import { StudentModel } from '../models/student'
import { TaskModel, Task } from '../models/task'

export default class TaskController {
  /**
   * Returns all tasks from the system or tasks of one student if the user requests an ID.
   *
   * @param ctx Koa context
   */
  public static async index(ctx) {
    let tasks: Task[] = []

    try {
      // if user requests a specific user id, get only that students tasks
      if (ctx.query.id) {
        const student = await StudentModel.findById(ctx.query.id)
        tasks = student && student.tasks ? student.tasks : []
      } else {
        // create a list of all tasks
        const students = await StudentModel.find({})

        for (const student of students) {
          if (student.tasks) {
            tasks = tasks.concat(student.tasks)
          }
        }
      }

      ctx.status = 200
      ctx.body = tasks
    } catch (err) {
      ctx.status = 400
      ctx.body = err
    }
  }

  /**
   * Update a task's notes.
   *
   * @param ctx Koa context
   */
  public static async update(ctx) {
    await StudentModel.findOneAndUpdate(
      { _id: ctx.request.body.studentId, 'tasks._id': ctx.params.id },
      {
        $set: {
          'tasks.$.notes': ctx.request.body.notes
        }
      },
      { new: true }
    )
      .then(async student => {
        if (!student) {
          ctx.status = 400
          ctx.body = `No task found for studentId ${
            ctx.request.body.studentId
          } and taskId ${ctx.params.id}`
        } else {
          ctx.status = 200
          ctx.body = await StudentModel.findOne({
            _id: ctx.request.body.studentId,
            'tasks._id': ctx.params.id
          })
        }
      })
      .catch(err => {
        ctx.status = 400
        ctx.body = err
      })
  }

  /**
   * Adds a new task.
   *
   * @param ctx Koa context
   */
  public static async store(ctx) {
    const data = ctx.request.body
    const studentId = data.studentId
    const newTask = new TaskModel({
      startDate: data.start,
      endDate: data.end,
      notes: data.notes,
      title: data.title
    })

    const studentToUpdate = await StudentModel.findById(studentId)
    if (studentToUpdate) {
      // if there are no tasks, initialize the array
      if (!studentToUpdate.tasks) {
        studentToUpdate.tasks = []
      }

      studentToUpdate.tasks.push(newTask)
      await studentToUpdate.save().then(() => {
        ctx.status = 200
        ctx.body = {
          studentId: studentId,
          _id: newTask.id,
          start: newTask.startDate,
          end: newTask.endDate,
          notes: newTask.notes,
          title: newTask.title
        }
      })
    } else {
      ctx.status = 400
      ctx.body = `No student found for id ${studentId}`
    }
  }

  /**
   * Deletes a task.
   *
   * @param ctx Koa context
   */
  public static async delete(ctx) {
    const id = ctx.params.id

    await StudentModel.findByIdAndUpdate(
      ctx.request.body.studentId,
      {
        $pull: {
          tasks: {
            _id: id
          }
        }
      },
      { new: true }
    ).then(stud => {
      if (stud) {
        ctx.status = 200
      } else {
        ctx.status = 400
        ctx.body = `No student found for id ${ctx.request.body.studentId}`
      }
    })
  }
}
