import jsonwebtoken from 'jsonwebtoken'
import bcrypt from 'bcrypt'
import { UserModel } from '../models/user'

export default class AuthenticationController {
  /**
   * Attempt to log in
   *
   * @param ctx Koa context
   */
  public static async login(ctx) {
    const user = await UserModel.findOne({
      email: ctx.request.body.email
    }).exec()

    if (!user) {
      ctx.status = 401
      ctx.body = 'Bad email'
    } else if (await bcrypt.compare(ctx.request.body.password, user.password)) {
      ctx.status = 200
      ctx.body = {
        token: jsonwebtoken.sign(
          {
            data: ctx.request.body.email
          },
          process.env.JWT_SECRET!,
          { expiresIn: '2h' }
        )
      }
    } else {
      ctx.status = 401
      ctx.body = 'Bad password'
    }
  }

  /**
   * Get user data
   *
   * @param ctx Koa context
   */
  public static async user(ctx) {
    await UserModel.findOne({ email: ctx.request.body.email })
      .then(usr => {
        if (!usr) {
          ctx.status = 401
          ctx.body = 'Bad email'
        } else {
          ctx.status = 200
          ctx.body = usr
        }
      })
      .catch(err => {
        ctx.status = 401
        ctx.body = err
      })
  }
}
