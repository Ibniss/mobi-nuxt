import { StudentModel } from '../models/student'

export default class StudentController {
  /**
   * Returns all students sorted by their surname.
   *
   * @param ctx Koa context
   */
  public static async index(ctx) {
    await StudentModel.find({})
      .sort({ surname: 'ascending' })
      .then(stud => {
        ctx.status = 200
        ctx.type = 'application/json'
        ctx.body = stud
      })
      .catch(err => {
        ctx.status = 400
        ctx.body = err
      })
  }

  /**
   * Adds a new student to the database.
   * @param ctx Koa context
   */
  public static async store(ctx) {
    const data = ctx.request.body

    const student = new StudentModel({
      name: data.name,
      surname: data.surname,
      age: data.age,
      city: data.city,
      street: data.street,
      home_number: data.home_number,
      contact_details: data.contact_details
    })

    await student
      .save()
      .then(stud => {
        ctx.status = 200
        ctx.type = 'application/json'
        ctx.body = stud
      })
      .catch(err => {
        ctx.status = 400
        ctx.body = err
      })
  }

  /**
   * Updates a student with new data.
   *
   * @param ctx Koa context
   */
  public static async update(ctx) {
    const data = ctx.request.body
    const id = ctx.params.id

    await StudentModel.findByIdAndUpdate(
      id,
      {
        $set: {
          name: data.name,
          surname: data.surname,
          age: data.age,
          city: data.city,
          street: data.street,
          home_number: data.home_number,
          contact_details: data.contact_details
        }
      },
      { new: true }
    )
      .then(stud => {
        if (stud) {
          ctx.status = 200
          ctx.type = 'application/json'
          ctx.body = stud
        } else {
          ctx.status = 400
          ctx.body = `No student found with id ${id}`
        }
      })
      .catch(err => {
        ctx.status = 400
        ctx.body = err
      })
  }

  /**
   * Deletes a student.
   *
   * @param ctx Koa context
   */
  public static async destroy(ctx) {
    await StudentModel.findByIdAndDelete(ctx.params.id)
      .then(stud => {
        if (!stud) {
          ctx.status = 400
          ctx.body = `No student found with id ${ctx.params.id}`
        } else {
          ctx.status = 200
        }
      })
      .catch(err => {
        ctx.status = 400
        ctx.body = err
      })
  }
}
