import axios from 'axios'
import { CommentModel } from '../models/comment'

export default class CommentController {
  /**
   * Returns all comments starting from the newest one.
   *
   * @param ctx Koa context
   */
  public static async index(ctx) {
    await CommentModel.find({})
      .sort({
        created_at: 'descending'
      })
      .then(com => {
        ctx.type = 'application/json'
        ctx.status = 200
        ctx.body = com
      })
      .catch(err => {
        ctx.status = 400
        ctx.body = err
      })
  }

  /**
   * Adds a new comment after verifying recaptcha.
   *
   * @param ctx Koa context
   */
  public static async store(ctx) {
    const recaptchaToken = ctx.request.body.recaptchaToken

    await axios
      .post('https://www.google.com/recaptcha/api/siteverify', {
        secret: 'TODO: captcha secret',
        response: recaptchaToken
      })
      .then(async ({ data }) => {
        // if the request was verified, add the comment
        if (data.success) {
          const comment = new CommentModel({
            body: ctx.request.body.bodyInput,
            author: ctx.request.body.authorInput,
            rating: ctx.request.body.ratingInput
          })
          await comment.save().then(com => {
            // TODO: send mail
            ctx.status = 200
            ctx.type = 'application/json'
            ctx.body = {
              _id: com.id,
              success: data.success,
              google_response: data
            }
          })
        } else {
          // else just send the response and don't do anything
          ctx.body = {
            success: data.success,
            google_response: data
          }
        }
      })
      .catch(err => {
        ctx.status = 400
        ctx.body = err
      })
  }

  /**
   * Deletes a comment.
   *
   * @param ctx Koa context
   */
  public static async destroy(ctx) {
    await CommentModel.findByIdAndRemove(ctx.params.id)
      .then(com => {
        if (!com) {
          ctx.status = 400
          ctx.body = `No comment found with id ${ctx.params.id}`
        } else {
          ctx.status = 200
        }
      })
      .catch(err => {
        ctx.status = 400
        ctx.body = err
      })
  }
}
