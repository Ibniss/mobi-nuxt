import Router from 'koa-router'
import CommentController from '../controllers/CommentController'
import TaskController from '../controllers/TaskController'
import StudentController from '../controllers/StudentController'
import jwt from '../middleware/jwt'
import AuthenticationController from '../controllers/AuthenticationController'

const router = new Router()

router
  .post('/auth/login', async (ctx, next) => {
    await AuthenticationController.login(ctx)
  })
  .get('/auth/user', jwt, async (ctx, next) => {
    await AuthenticationController.user(ctx)
  })
  .get('/comments', async (ctx, next) => {
    await CommentController.index(ctx)
  })
  .post('/comments', async (ctx, next) => {
    await CommentController.store(ctx)
  })
  .delete('/comments/:id', jwt, async (ctx, next) => {
    await CommentController.destroy(ctx)
  })
  .get('/tasks', jwt, async (ctx, next) => {
    await TaskController.index(ctx)
  })
  .post('/tasks', jwt, async (ctx, next) => {
    await TaskController.store(ctx)
  })
  .patch('/tasks/:id', jwt, async (ctx, next) => {
    await TaskController.update(ctx)
  })
  .delete('/tasks/:id', jwt, async (ctx, next) => {
    await TaskController.delete(ctx)
  })
  .get('/students', jwt, async (ctx, next) => {
    await StudentController.index(ctx)
  })
  .post('/students', jwt, async (ctx, next) => {
    await StudentController.store(ctx)
  })
  .patch('/students/:id', jwt, async (ctx, next) => {
    await StudentController.update(ctx)
  })
  .delete('/students/:id', jwt, async (ctx, next) => {
    await StudentController.destroy(ctx)
  })

export default router
