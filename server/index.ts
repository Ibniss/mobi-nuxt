import Koa from 'koa'
import consola from 'consola'
import { Nuxt, Builder } from 'nuxt'
import 'reflect-metadata'
import koaNuxt from '@hiswe/koa-nuxt'
import config from '../nuxt.config'
import setupDb from './db'

// Import and Set Nuxt.js options
import middleware from './middleware'

const app = new Koa()
const { ready } = consola
const dev = !(app.env === 'production')

async function start() {
  // Instantiate nuxt.js
  const nuxt = new Nuxt(config)
  const renderNuxt = koaNuxt(nuxt)

  const {
    host = process.env.HOST || '127.0.0.1',
    port = process.env.PORT || 3000
  } = nuxt.options.server

  // if we are not in a test environment, set up a real database connection
  if (process.env.JEST_WORKER_ID === undefined) {
    setupDb()
  }

  // Build in development
  if (dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }

  // Apply all middlewares
  middleware(app)

  app.use(renderNuxt)

  app.listen(port, host)
  ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}

start()
