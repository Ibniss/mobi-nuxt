/* eslint-disable camelcase */
import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import axios from '~/plugins/axios'

export interface Task {
  _id: string
  startDate: Date
  endDate: Date
  notes: string
  title: string
}

export interface Student {
  _id: string
  name: string
  surname: string
  age: Number
  city?: string
  street?: string
  home_number?: Number
  contact_details?: string
  created_at: Date
  updated_at: Date
  tasks: Task[]
}

export interface TaskUpdate {
  studentId: string
  notes: string
}

export interface NewTask {
  studentId: string
  _id: string
  start: Date
  end: Date
  notes: string
  title: string
}

export interface DeleteTask {
  studentId: string
  taskId: string
}

@Module({ namespaced: true, name: 'students', stateFactory: true })
export default class StudentsModule extends VuexModule {
  students: Student[] = []
  currentStudent: Student = {
    _id: '',
    name: '',
    surname: '',
    age: 0,
    created_at: new Date(),
    updated_at: new Date(),
    tasks: [],
    city: '',
    street: '',
    home_number: 0,
    contact_details: ''
  }

  get allTasks(): Task[] {
    return this.students.flatMap(stud => stud.tasks)
  }

  get studentTasks() {
    return (studentId: string) => {
      const foundStudent = this.students.find(stud => stud._id === studentId)
      return foundStudent ? foundStudent.tasks : []
    }
  }

  get studentById() {
    return (studentId: string) => {
      return this.students.find(stud => stud._id === studentId)
    }
  }

  /**
   * Current student mutations.
   */

  @Mutation
  setCurrentStudent(id: string) {
    this.currentStudent = this.studentById(id) || this.currentStudent
  }

  @Mutation
  setCurrentStudentName(name: string) {
    this.currentStudent.name = name
  }

  @Mutation
  setCurrentStudentSurname(surname: string) {
    this.currentStudent.surname = surname
  }

  @Mutation
  setCurrentStudentAge(age: Number) {
    this.currentStudent.age = age
  }

  @Mutation
  setCurrentStudentHome(home: Number) {
    this.currentStudent.home_number = home
  }

  @Mutation
  setCurrentStudentCity(city: string) {
    this.currentStudent.city = city
  }

  @Mutation
  setCurrentStudentStreet(street: string) {
    this.currentStudent.street = street
  }

  @Mutation
  setCurrentStudentContact(contact: string) {
    this.currentStudent.contact_details = contact
  }

  @Mutation
  setCurrentStudentsTaskNotes(id: string, notes: string) {
    const taskToUpdate = this.currentStudent.tasks.find(task => task._id === id)
    if (taskToUpdate) {
      taskToUpdate.notes = notes
    }
  }

  /**
   * General students state mutations.
   */

  @Mutation
  setStudents(studs: Student[]) {
    this.students = studs
  }

  @Mutation
  addStudent(stud: Student) {
    this.students.push(stud)
  }

  @Mutation
  removeStudent(studId: string) {
    this.students = this.students.filter(stud => stud._id !== studId)
  }

  @Mutation
  updateStudent(stud: Student) {
    const index = this.students.findIndex(s => s._id === stud._id)
    this.students[index] = stud
  }

  /**
   * General tasks state mutations.
   */

  @Mutation
  addTask(task: NewTask) {
    const toModify = this.students.find(stud => stud._id === task.studentId)
    if (toModify) {
      const newTask: Task = {
        _id: task._id,
        startDate: task.start,
        endDate: task.end,
        notes: task.notes,
        title: task.notes
      }
      toModify.tasks.push(newTask)
    }
  }

  @Mutation
  removeTask(toDelete: DeleteTask) {
    const toDeleteFrom = this.students.find(
      stud => stud._id === toDelete.studentId
    )
    if (toDeleteFrom) {
      toDeleteFrom.tasks = toDeleteFrom.tasks.filter(
        task => task._id !== toDelete.taskId
      )
    }
  }

  /**
   * Student actions.
   */

  @Action({ commit: 'setStudents' })
  async fetch() {
    const response = await axios.get('/students').catch(err => {
      throw err
    })
    return response.data
  }

  @Action({ commit: 'addStudent' })
  async store(stud: Student) {
    const response = await axios.post('/students', stud).catch(err => {
      throw err
    })
    this.context.commit('setCurrentStudent', this.currentStudent._id)
    return response.data
  }

  @Action({ commit: 'updateStudent' })
  async update(stud: Student) {
    const response = await axios
      .patch(`/students/${stud._id}`, stud)
      .catch(err => {
        throw err
      })
    this.context.commit('setCurrentStudent', this.currentStudent._id)
    return response.data
  }

  @Action({ commit: 'removeStudent' })
  async delete(studId: string) {
    await axios.delete(`/students/${studId}`).catch(err => {
      throw err
    })
    this.context.commit('setCurrentStudent', this.currentStudent._id)
    return studId
  }

  /**
   * Task actions.
   */

  @Action({ commit: 'updateStudent' })
  async updateTask(updt: TaskUpdate, taskId: string) {
    const response = await axios.patch(`/tasks/${taskId}`, updt).catch(err => {
      throw err
    })
    this.context.commit('setCurrentStudent', this.currentStudent._id)
    return response.data
  }

  @Action({ commit: 'addTask' })
  async storeTask(task: NewTask) {
    const response = await axios.post('/tasks', task).catch(err => {
      throw err
    })
    this.context.commit('setCurrentStudent', this.currentStudent._id)
    return response.data
  }

  @Action({ commit: 'removeTask' })
  async deleteTask(taskId: string, studId: string) {
    await axios
      .delete(`/tasks/${taskId}`, {
        data: {
          studentId: studId
        }
      })
      .catch(err => {
        throw err
      })
    const rtrn: DeleteTask = {
      taskId: taskId,
      studentId: studId
    }

    this.context.commit('setCurrentStudent', this.currentStudent._id)
    return rtrn
  }
}
