import axios from '~/plugins/axios'
import { VuexModule, Module, Action, Mutation } from 'vuex-module-decorators'

export interface Comment {
  _id: string
  body: string
  author: string
  rating: Number
}

export interface CommentRequest {
  token: string
  bodyInput: string
  authorInput: string
  ratingInput: Number
}

@Module({ namespaced: true, name: 'comments', stateFactory: true })
export default class CommentsModule extends VuexModule {
  comments: Comment[] = []

  @Mutation
  setComments(comments: Comment[]) {
    this.comments = comments
  }

  @Mutation
  addComment(comment: Comment) {
    this.comments.push(comment)
  }

  @Action({ commit: 'setComments' })
  async fetch() {
    const response = await axios.get('/comments').catch(err => {
      throw err
    })
    return response.data
  }

  @Action
  async store(request: CommentRequest) {
    const response = await axios.post('/comments', request).catch(err => {
      throw err
    })
    this.context.commit('addComment', {
      _id: response.data._id,
      body: request.bodyInput,
      author: request.authorInput,
      rating: request.ratingInput
    })
    return response.data
  }

  @Action
  async delete(id: string) {
    const response = await axios.delete(`/comments/${id}`).catch(err => {
      throw err
    })
    return response.data
  }
}
