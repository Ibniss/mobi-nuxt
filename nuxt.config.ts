const pkg = require('./package')

export default {
  debug: true,
  env: {
    BASE_URL: process.env.BASE_URL || 'http://localhost:3000'
  },
  mode: 'universal',

  /*
   ** Headers of the page
   */
  head: {
    title:
      'Szkoła językowa MOBI - język angielski - Kęty, Andrychów, Kobiernice, Czaniec, Porąbka',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description },
      {
        name: 'keywords',
        content:
          'Katarzyna Harat, Katarzyna Bielikowicz, szkoła językowa, mobilna szkoła językowa, Kęty, Andrychów, Kobiernice, Czaniec, Porąbka, Kozy, Bulowice, angielski, nauczanie angielskiego.'
      }
    ],
    link: [
      { rel: 'icon', href: 'https://mobi.edu.pl/img/logo-no-background.png' },
      // {
      //   rel: 'stylesheet',
      //   href:
      //     'https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css'
      // },
      {
        rel: 'stylesheet',
        href:
          '//cdn.materialdesignicons.com/2.0.46/css/materialdesignicons.min.css'
      },
      {
        rel: 'stylesheet',
        href: '//fonts.googleapis.com/css?family=Josefin+Sans'
      }
    ],
    script: [
      {
        src: 'https://use.fontawesome.com/releases/v5.0.6/js/all.js',
        defer: true
      }
      // {
      //   src:
      //     'https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit',
      //   async: true,
      //   defer: true
      // }
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },

  /*
   ** Global CSS
   */
  css: [],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '~/plugins/vue-paginate', ssr: false },
    { src: '~/plugins/vue-carousel', ssr: false },
    { src: '~/plugins/vue-collapse', ssr: false },
    { src: '~/plugins/buefy', ssr: false }
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc:https://github.com/nuxt-community/modules/tree/master/packages/bulma
    '@nuxtjs/bulma',
    // '@nuxtjs/pwa',
    '@nuxtjs/auth',
    '@nuxtjs/axios'
  ],

  /**
   * Auth module configuration
   */
  // auth: { redirect: {
  //     login: '/login',
  //     logout: '/',
  //     callback: '/login',
  //     home: '/'
  //   },
  //   strategies: {
  //     local: {
  //       endpoints: {
  //         login: { url: '/auth/login', method: 'post', propertyName: 'token' },
  //         user: { url: '/auth/user', method: 'get', propertyName: undefined }
  //       }
  //     }
  //   }
  // },

  /*
   ** Build configuration
   */
  build: {
    babel: {
      plugins: [
        ['@babel/plugin-proposal-decorators', { legacy: true }],
        ['@babel/plugin-proposal-class-properties', { loose: true }]
      ]
    },
    postcss: {
      preset: {
        features: {
          customProperties: false
        }
      }
    },
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
